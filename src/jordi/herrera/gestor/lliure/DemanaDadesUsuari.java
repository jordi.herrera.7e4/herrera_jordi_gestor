package jordi.herrera.gestor.lliure;

import java.util.Scanner;

public class DemanaDadesUsuari {

    static public int demanaEnter(String text, Scanner sc) {
        System.out.println(text);
        while (!sc.hasNextInt()) {
            System.out.println("[error] no és un enter");
            sc.next();
        }
        return sc.nextInt();
    }

    static public int demanaEnterRang(String text, Scanner sc, int min, int max) {
        int num = demanaEnter(text, sc);
        while (num < min || num > max) {
            num = demanaEnter(text, sc);
        }
        return num;
    }

    static public int primerReal(String text, Scanner sc) {
        int num = demanaEnter(text, sc);
        return num;
    }
}
