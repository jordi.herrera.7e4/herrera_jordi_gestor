package jordi.herrera.gestor.lliure;

import java.util.Arrays;
import java.util.Scanner;

public class Gestor {
    private int[] punts;
    private String[] noms;
    public void imprimeixArrayNumeric (){
        System.out.println(punts.toString());
    }
    public void imprimeixArrayDeNoms (){
        Arrays.sort(noms);
        System.out.println(noms.toString());
    }

    public void emplenarArrayNumeric (Scanner sc, int num){
        punts = new int[num];

        for(int i = 0; i < num; i++){
            punts[i] = DemanaDadesUsuari.demanaEnter("Introdueix els punts: ", sc);
        }
    }
    public void emplenarArrayDeNoms (Scanner sc, int num){
        noms = new String[num];

        for(int i = 0; i < num; i++){
            System.out.println("Introdueix els noms: ");
            noms[i] = sc.nextLine();
        }
    }
}
