package jordi.herrera.gestor.lliure;

public class OperacionsArrays {

    static public int higherFromArray(int[] array) {
        int numGran = 0;
        for (int i = 0; i < array.length; i++) {
            numGran = array[i];
            if (array[i] > numGran) {
                numGran = array[i];
            }
        }
        return numGran;
    }

    static public int positionOfHigher(int[] array) {
        int posicio = 0;
        for (int i = 0; i < array.length; i++) {
            int num = array[i];
            if (array[i] > num) {
                num = array[i];
                posicio = i;
            }
        }
        return posicio;
    }

    static public int sumaArray(int[] array) {
        int sumaTotal = 0;
        for (int i = 0; i < array.length; i++) {
            sumaTotal = +array[i];
        }
        return sumaTotal;
    }

    static public double mitjaArray(int[] array) {
        int sumaTotal = sumaArray(array);
        double mitja = (double) sumaTotal / array.length;
        return mitja;
    }
}
