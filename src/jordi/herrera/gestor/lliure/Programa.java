package jordi.herrera.gestor.lliure;

import java.util.Scanner;

public class Programa {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int opcio = DemanaDadesUsuari.demanaEnterRang("Introdueix un num entre 0 i 8: ", sc, 0, 8);

        System.out.println("opcio = " + opcio);

    }
}
